import os
import logging
from datetime import datetime

from slack_bolt.app import App
from slack_bolt.error import BoltError
from slack_bolt.adapter.socket_mode import SocketModeHandler
from slack.slack_command import SlackCommand, SlackCommandException
from slack_sdk.errors import SlackApiError

from utils.dates import (
    validate_year_month,
    month_year_to_date_range,
    validate_year_month,
)
from billing.accounts import Accounts
from billing.services import Services

# Initialize logging
logging.basicConfig(
    format="%(message)s", level=os.environ.get("LOGLEVEL", "INFO").upper()
)
logger = logging.getLogger(__name__)

slack_bot_token = os.getenv("SLACK_BOT_TOKEN", "")
app = App(token=slack_bot_token)

aws_report_args = [
    {
        "name": "report_type",
        "description": "The type of report to generate (e.g. accounts, services)",
        "valid_values": ["accounts", "services"],
        "type": str,
        "required": True,
    },
    {
        "name": "month",
        "description": "The month to get costs for (e.g. April, May, June, etc.)",
        "type": str,
        "required": True,
    },
    {
        "name": "year",
        "description": "The year to get costs for (e.g. 2021)",
        "type": int,
        "required": False,
        "default": datetime.now().year,
    },
    {
        "name": "account_id",
        "description": "The AWS account ID to get costs for (e.g. 123456789012)",
        "type": str,
        "required": False,
    },
]


@app.command("/aws-report")
def aws(ack, client, command):
    ack()

    try:
        slack_command = SlackCommand(client, command, aws_report_args)
    except SlackCommandException as e:
        client.chat_postMessage(channel=command["user_id"], text=str(e))
        return

    if slack_command["report_type"] == "accounts":
        accounts(client, slack_command)
    elif slack_command["report_type"] == "services":
        services(client, slack_command)
    else:
        client.chat_postMessage(
            channel=command["user_id"],
            text=f"Invalid report type: {slack_command['report_type']}",
        )


def accounts(client, command):
    try:
        (month, year) = validate_year_month(command=command)
    except ValueError as e:
        client.chat_postMessage(channel=command.user_id, text=str(e))
        return

    (start_date, end_date) = month_year_to_date_range(month, year)

    client.chat_postMessage(
        channel=command.user_id,
        text=f"Getting costs by account for {month}/{year}...",
    )

    accounts = Accounts()
    cost_by_account = accounts.get_cost_and_usage(start_date, end_date)

    buffer = ""
    for row in cost_by_account:
        buffer += row + "\n"

    try:
        response = client.files_upload_v2(
            channel=command.channel_id,
            content=buffer,
            filename=f"cost-by-account-{month}-{year}.csv",
            initial_comment="Here's the report you requested!",
        )

    except SlackApiError as e:
        # Handle any Slack API errors
        error_message = e.response["error"]
        client.chat_postMessage(
            channel=command.user_id,
            text=f"An error occurred while uploading the file: {error_message}",
        )

        raise e


def services(client, command):
    try:
        (month, year) = validate_year_month(command=command)
    except ValueError as e:
        client.chat_postMessage(channel=command.user_id, text=str(e))
        return

    (start_date, end_date) = month_year_to_date_range(month, year)

    client.chat_postMessage(
        channel=command.channel_id,
        text=f"Getting costs by service for {month}/{year}...",
    )

    services = Services()

    cost_by_service = services.get_cost_and_usage(start_date, end_date)

    buffer = ""

    for row in cost_by_service:
        buffer += row + "\n"

    try:
        response = client.files_upload_v2(
            channel=command.channel_id,
            content=buffer,
            filename=f"cost-by-service-{month}-{year}.csv",
            initial_comment="Here's the report you requested!",
        )

    except SlackApiError as e:
        # Handle any Slack API errors
        error_message = e.response["error"]
        client.chat_postMessage(
            channel=command.user_id,
            text=f"An error occurred while uploading the file: {error_message}",
        )


@app.error
def global_error_handler(error, body, logger):
    logger.exception(error)
    logger.info(body)


def main():
    slack_app_token = os.getenv("SLACK_APP_TOKEN", "")
    SocketModeHandler(app, logger=logger, app_token=slack_app_token).start()


if __name__ == "__main__":
    main()
