import calendar
from datetime import datetime, timedelta
from typing import Union

from slack.slack_command import SlackCommand


def validate_year_month(command: SlackCommand) -> Union[int, int]:
    """Validate the year and month arguments of a Slack command.

    Args:
        command (SlackCommand): The Slack command.

    Returns:
        tuple: A tuple containing the month and year.

    Raises:
        ValueError: If the month or year is invalid.
    """

    month = None

    # Simple number validation
    if command["month"] not in range(1, 13):
        month = convert_month_to_number(command["month"])

    # Month is None if the month name is invalid
    if month is None:
        raise ValueError(f"Invalid month: {month}")

    # Simple year validation
    if command["year"] not in range(2020, datetime.now().year + 1):
        raise ValueError(
            f"Invalid year: {command['year']} must be between 2020 and {datetime.now().year}"
        )

    return (month, command["year"])


def convert_month_to_number(month_name) -> int:
    # Get the mapping of month names to numbers (full names)
    month_mapping = {
        month.lower(): index for index, month in enumerate(calendar.month_name) if month
    }

    # Get the mapping of month names to numbers (abbreviated names)
    month_mapping_abbr = {
        month.lower(): index for index, month in enumerate(calendar.month_abbr) if month
    }

    # Convert the month name to lowercase
    month_name_lower = month_name.lower()

    # Look up the corresponding month number (full names)
    month_number = month_mapping.get(month_name_lower)

    if month_number is None:
        # Look up the corresponding month number (abbreviated names)
        month_number = month_mapping_abbr.get(month_name_lower)

    return month_number


def get_first_day_of_next_month(start_date) -> datetime:
    # Calculate the next month, account for february
    next_month = start_date.replace(day=28) + timedelta(days=4)

    # If the next month is december, set the year to the next year and set the month to january
    if next_month.month == 12:
        next_month = next_month.replace(year=next_month.year + 1, month=1)
    else:
        next_month = next_month.replace(month=next_month.month)

    # Set the day to 1 to get the first day of the next month
    first_day_of_next_month = next_month.replace(day=1)

    return first_day_of_next_month


def month_year_to_date_range(month, year) -> Union[datetime, datetime]:
    # Create a start date and end date
    start_date = datetime(year, month, 1)
    end_date = get_first_day_of_next_month(start_date)

    return (start_date, end_date)
