import datetime


class SlackCommandException(Exception):
    pass


class SlackCommand:
    def __init__(self, client, command, expected_args: list = None):
        self.client = client
        self.command = command
        self.user_id = command["user_id"]
        self.channel_id = command["channel_id"]
        self.expected_args = expected_args

        # Parse the command arguments
        if self.expected_args is not None:
            self.__parse_args()

    def __parse_args(self):
        self.args = {}
        args = self.command.get("text", "").split()

        # Check if the number of arguments is correct
        if len(args) < len([arg for arg in self.expected_args if arg["required"]]):
            required_args = " ".join(
                [f"{arg['name']}" for arg in self.expected_args if arg["required"]]
            )
            optional_args = " ".join(
                [
                    f"[{arg['name']}]"
                    for arg in self.expected_args
                    if not arg["required"]
                ]
            )
            arg_help = "\n".join(
                [
                    "*{}*".format(arg["name"]) + ": " + arg["description"]
                    for arg in self.expected_args
                ]
            )
            raise SlackCommandException(
                f"""
`{self.command['command']} {required_args} {optional_args}`\n
{arg_help}\n
Example: _/aws-report accounts April 2021_
""",
            )

        # Validate the arguments
        for i, arg in enumerate(args):
            expected_arg = self.expected_args[i]
            if expected_arg["type"] == str:
                continue
            elif expected_arg["type"] == int:
                try:
                    args[i] = int(arg)
                except ValueError:
                    raise SlackCommandException(
                        message=f"Please specify a valid {expected_arg['name']}"
                    )
            elif expected_arg["type"] == datetime.datetime:
                try:
                    args[i] = datetime.datetime.strptime(arg, "%Y-%m-%d")
                except ValueError:
                    raise SlackCommandException(
                        message=f"Please specify a valid {expected_arg['name']} (e.g. 2021-01-01)"
                    )

        # Create a dictionary of the arguments
        self.args = {
            expected_arg["name"]: arg
            for expected_arg, arg in zip(self.expected_args, args)
        }

        return args

    def __str__(self):
        return f"SlackCommand(client={self.client}, command={self.command}, user_id={self.user_id}, channel_id={self.channel_id}, args={self.args})"

    def __repr__(self):
        return self.__str__()

    def __getitem__(self, key):
        return (
            self.args[key]
            if key in self.args
            else [arg["default"] for arg in self.expected_args if arg["name"] == key][0]
        )
