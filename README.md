# Cloud Billing Bot

Easily gather billing information from your cloud provider(s) from the convenience of Slack.

## Setup

### Slack

1. Create a new Slack app
2. Add a bot user
3. Install the app to your workspace
4. Copy the bot token
5. Add the bot to the channels you want it to be able to post to

Environment based configuration:

```bash
export SLACK_APP_TOKEN=<your slack app token>
export SLACK_BOT_TOKEN=<your slack bot token>
```

### AWS

Coming soon, currently the bot only runs within the global credentials context of the host machine.

## Running

Run it locally:

```bash
poetry run python main.py
```

Run it in a container (recommended):

```bash
docker build -t cloud-billing-bot .
docker run -it --rm -e SLACK_APP_TOKEN=$SLACK_APP_TOKEN -e SLACK_BOT_TOKEN=$SLACK_BOT_TOKEN cloud-billing-bot
```