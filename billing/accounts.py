from datetime import datetime
import boto3


class Accounts:
    """
    Accounts class for getting AWS billing information related to linked accounts
    """

    def __init__(self):
        self.client = boto3.client("ce")

    def get_cost_and_usage(
        self,
        start_date: datetime,
        end_date: datetime,
        granularity: str = "DAILY",
        metrics: list = ["UnblendedCost"],
    ) -> list:
        data = self.__fetch_data(start_date, end_date, granularity, metrics)
        results = self.__parse_data(data)

        return results

    def __fetch_data(
        self,
        start_date: datetime,
        end_date: datetime,
        granularity: str,
        metrics: list,
    ) -> dict:
        start_string = start_date.strftime("%Y-%m-%d")
        end_string = end_date.strftime("%Y-%m-%d")

        response = self.client.get_cost_and_usage(
            TimePeriod={"Start": start_string, "End": end_string},
            Granularity=granularity,
            Metrics=metrics,
            GroupBy=[
                {"Type": "DIMENSION", "Key": "LINKED_ACCOUNT"},
            ],
            Filter={
                "Not": {
                    "Dimensions": {"Key": "RECORD_TYPE", "Values": ["Refund", "Credit"]}
                }
            },
        )

        return response

    def __parse_data(self, data: dict) -> list:
        linked_accounts = {
            attribute["Value"]: attribute["Attributes"]["description"]
            for attribute in data["DimensionValueAttributes"]
        }

        linked_accounts["total"] = "Total Costs"

        account_ids = [
            attribute["Value"] for attribute in data["DimensionValueAttributes"]
        ]
        account_names = [
            attribute["Attributes"]["description"]
            for attribute in data["DimensionValueAttributes"]
        ]

        daily = {}
        account_total = {}
        for result_by_time in data["ResultsByTime"]:
            date = result_by_time["TimePeriod"]["Start"]
            daily[date] = {}
            daily[date]["total"] = 0.0
            for group in result_by_time["Groups"]:
                amount = float(group["Metrics"]["UnblendedCost"]["Amount"])
                daily[date][linked_accounts[group["Keys"][0]]] = amount
                daily[date]["total"] += amount

                if linked_accounts[group["Keys"][0]] not in account_total:
                    account_total[linked_accounts[group["Keys"][0]]] = 0.0

                account_total[linked_accounts[group["Keys"][0]]] += amount

        account_totals = []
        for account_name in account_names:
            account_totals.append(
                account_total[account_name] if account_name in account_total else 0.0
            )

        results = [
            '"Linked account",{},""'.format(
                ",".join(['"{}"'.format(x) for x in account_ids])
            ),
            '"Linked account name",{},Total costs ($)"'.format(
                ",".join(['"{} ($)"'.format(x) for x in account_names])
            ),
            '"Linked account total",{},"{}"'.format(
                ",".join(['"{}"'.format(x) for x in account_totals]),
                sum(account_totals),
            ),
        ]

        for date in daily:
            results.append(
                '"{}",{},"{}"'.format(
                    date,
                    ",".join(
                        [
                            '"{}"'.format(daily[date][account_name])
                            if account_name in daily[date]
                            else '"0.0"'
                            for account_name in account_names
                        ]
                    ),
                    daily[date]["total"],
                )
            )

        return results
